package cz.eman.marekkodr.eman.fragment


import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import com.bumptech.glide.Glide
import cz.eman.marekkodr.eman.R
import cz.eman.marekkodr.eman.activity.ImageActivity
import cz.eman.marekkodr.eman.activity.MainActivity
import cz.eman.marekkodr.eman.activity.OwnerActivity
import cz.eman.marekkodr.eman.base.BaseFragment
import cz.eman.marekkodr.eman.databinding.FragmentDetailBinding
import cz.eman.marekkodr.eman.holder.Fragment
import cz.eman.marekkodr.eman.viewmodel.DetailViewModel


class DetailFragment : BaseFragment() {

    private lateinit var viewModel : DetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding : FragmentDetailBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_detail,container , false)

        viewModel = ViewModelProviders.of(this).get(DetailViewModel::class.java)

        binding.item = viewModel.getItem()

        //if is tablet or not
        if(activity is MainActivity){
            binding.detailArrowBack.visibility = GONE
        }
        else{
            binding.detailArrowBack.setOnClickListener {
                activity!!.onBackPressed()
            }
        }

        binding.detailOwnerLayout.setOnClickListener {

            if(activity is MainActivity) {
                Fragment.fragment = OwnerFragment()
                (activity as MainActivity).setSideFragment()
            }
            else {
                val intent = Intent(activity, OwnerActivity::class.java)
                startActivity(intent)
            }
        }

        binding.detailOwnerImage.setOnClickListener {
            val intent = Intent(activity, ImageActivity::class.java)
            startActivity(intent)
        }

        //load img
        Glide.with(this)
                .load(viewModel.getOwnerUrl())
                .apply(getRequestOptions())
                .into(binding.detailOwnerImage)

        return binding.root
    }
}
