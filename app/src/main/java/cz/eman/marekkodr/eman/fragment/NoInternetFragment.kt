package cz.eman.marekkodr.eman.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import cz.eman.marekkodr.eman.R
import cz.eman.marekkodr.eman.activity.MainActivity


class NoInternetFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val inflate = inflater.inflate(R.layout.fragment_no_internet, container, false)

        inflate.findViewById<Button>(R.id.fragment_loading_retry).setOnClickListener {

            (activity as MainActivity).checkConnection()
        }

        return inflate
    }
}
