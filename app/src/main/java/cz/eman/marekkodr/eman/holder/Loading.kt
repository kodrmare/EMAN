package cz.eman.marekkodr.eman.holder

object Loading {

    var has_more : Boolean = true
    var quota_max : Int = 0
    var quota_remaining : Int = 0

}