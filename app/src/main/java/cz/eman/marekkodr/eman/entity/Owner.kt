package cz.eman.marekkodr.eman.entity

import android.arch.persistence.room.Entity

@Entity
data class Owner (
    var display_name: String? = null,

    var user_type: String? = null,

    var profile_image: String? = null,

    var link: String? = null,

    var reputation: String? = null,

    var user_id: String? = null
)
