package cz.eman.marekkodr.eman.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import cz.eman.marekkodr.eman.R
import cz.eman.marekkodr.eman.fragment.OwnerFragment

class OwnerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_owner)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.activity_owner_frame, OwnerFragment())
        transaction.commit()
    }
}
