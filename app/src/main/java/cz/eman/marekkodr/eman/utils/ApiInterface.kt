package cz.eman.marekkodr.eman.utils

import cz.eman.marekkodr.eman.entity.ItemsResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    //https://api.stackexchange.com/2.2/questions?filter=withbody&order=desc&sort=creation&site=cooking&pagesize=5&page=1

    @GET("questions")
    fun getItems(
            @Query("filter") filter : String,
            @Query("order") order : String,
            @Query("sort") sort : String,
            @Query("site") site : String,
            @Query("pagesize") pagesize : Int,
            @Query("page") page : Int
                 ): Call<ItemsResponse>

    companion object Factory {
        private const val BASE_URL = "https://api.stackexchange.com/2.2/"
        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}