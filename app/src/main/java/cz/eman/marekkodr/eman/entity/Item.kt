package cz.eman.marekkodr.eman.entity

import android.arch.persistence.room.Entity
import java.util.*

@Entity
data class Item (
    var creation_date: String? = null,

    var tags: ArrayList<String>? = null,

    var body: String? = null,

    var title: String? = null,

    var link: String? = null,

    var score: String? = null,

    var answer_count: String? = null,

    var owner: Owner? = null,

    var last_activity_date: String? = null,

    var question_id: String? = null,

    var view_count: String? = null,

    //cannot start with "is" ... kotlin has problem...
    var is_answered: String? = null
)