package cz.eman.marekkodr.eman.base

import android.support.v4.app.Fragment
import com.bumptech.glide.request.RequestOptions
import cz.eman.marekkodr.eman.R

abstract class BaseFragment : Fragment() {

    protected fun getRequestOptions() : RequestOptions {
        val requestOptions = RequestOptions()
        requestOptions.placeholder(R.drawable.person)

        return requestOptions
    }
}