package cz.eman.marekkodr.eman.entity

import android.arch.persistence.room.Entity
import java.util.*

@Entity
data class ItemsResponse (
    var quota_max: String? = null,

    var items: ArrayList<Item>? = null,

    var has_more: String? = null,

    var quota_remaining: String? = null
)