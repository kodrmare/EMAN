package cz.eman.marekkodr.eman.viewmodel

import android.arch.lifecycle.ViewModel
import cz.eman.marekkodr.eman.entity.Owner
import cz.eman.marekkodr.eman.holder.Item

class OwnerViewModel : ViewModel() {

    fun getOwner() : Owner {
        return Item.item!!.owner!!
    }

    fun getOwnerUrl() : String
    {
        return Item.item!!.owner!!.profile_image.toString()
    }
}