package cz.eman.marekkodr.eman.activity

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.res.Configuration
import android.databinding.DataBindingUtil
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import cz.eman.marekkodr.eman.R
import cz.eman.marekkodr.eman.holder.Fragment
import cz.eman.marekkodr.eman.databinding.ActivityMainBinding
import cz.eman.marekkodr.eman.fragment.NoInternetFragment
import cz.eman.marekkodr.eman.fragment.NoItemFragment
import cz.eman.marekkodr.eman.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel : MainViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        binding.mainRecycler.layoutManager = LinearLayoutManager(this)

        main_recycler.layoutManager = LinearLayoutManager(this)

        viewModel.setAdapter(binding.mainRecycler, this)

        //check if connected to Internet
        checkConnection()

        //at first set empty sideFragment for tablet
        Fragment.fragment = NoItemFragment()
        setSideFragment()
    }

    //set fragment according to context
    fun setSideFragment(){
        if(isTablet(this)){
            val newFragment = Fragment.fragment!!
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.activity_main_fragment_detail, newFragment)
            transaction.commit()
        }
    }

    //handle situation if connected
    fun checkConnection(){
        if(isConnected()){
            binding.mainFragment.visibility = View.GONE
            viewModel.loadItems(this)
        }
        else{
            binding.mainFragment.visibility = View.VISIBLE
            val newFragment = NoInternetFragment()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.main_fragment, newFragment)
            transaction.commit()
        }
    }
    //check if connected to the Internet
    private fun isConnected() :Boolean{
        val cm = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo

        return activeNetwork?.isConnected == true
    }

    fun isTablet(context: Context): Boolean {
        val xlarge = context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == Configuration.SCREENLAYOUT_SIZE_XLARGE
        val large = context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == Configuration.SCREENLAYOUT_SIZE_LARGE
        return xlarge || large
    }
}
