package cz.eman.marekkodr.eman.viewmodel

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.view.Gravity
import android.widget.TextView
import android.widget.Toast
import cz.eman.marekkodr.eman.R

abstract class BaseViewModel : ViewModel() {

    fun showToastMessage(textToast: String, activity: Activity) {
        val inflater = activity.layoutInflater
        val layout = inflater.inflate(R.layout.toast, activity.findViewById(R.id.custom_toast_container))

        val text = layout.findViewById<TextView>(R.id.text)

        text.text = textToast
        val toast = Toast(activity)
        toast.setGravity(Gravity.BOTTOM, 0, 200)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }

}