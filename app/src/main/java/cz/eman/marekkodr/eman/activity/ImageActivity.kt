package cz.eman.marekkodr.eman.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView
import cz.eman.marekkodr.eman.R
import cz.eman.marekkodr.eman.holder.Item
import kotlinx.android.synthetic.main.activity_image.*


class ImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        val photoView = findViewById<PhotoView>(R.id.acitvity_image_photo)

        Glide.with(this)
                .load(Item.item!!.owner!!.profile_image)
                .into(photoView)


        image_arrow_back.setOnClickListener { onBackPressed() }
    }
}
