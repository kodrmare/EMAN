package cz.eman.marekkodr.eman.fragment


import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import cz.eman.marekkodr.eman.R
import cz.eman.marekkodr.eman.activity.ImageActivity
import cz.eman.marekkodr.eman.activity.MainActivity
import cz.eman.marekkodr.eman.base.BaseFragment
import cz.eman.marekkodr.eman.databinding.FragmentOwnerBinding
import cz.eman.marekkodr.eman.holder.Fragment
import cz.eman.marekkodr.eman.viewmodel.OwnerViewModel

class OwnerFragment : BaseFragment() {

    private lateinit var viewModel : OwnerViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val binding : FragmentOwnerBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_owner,container , false)

        viewModel = ViewModelProviders.of(this).get(OwnerViewModel::class.java)

        binding.owner = viewModel.getOwner()


        binding.ownerArrowBack.setOnClickListener {

            if(activity is MainActivity) {
                Fragment.fragment = DetailFragment()
                (activity as MainActivity).setSideFragment()
            }
            else{
                activity!!.onBackPressed()
            }
        }

        binding.ownerOwnerImg.setOnClickListener {
            val intent = Intent(activity, ImageActivity::class.java)
            startActivity(intent)
        }

        //load img
        Glide.with(this)
                .load(viewModel.getOwnerUrl())
                .apply(getRequestOptions())
                .into(binding.ownerOwnerImg)

        return binding.root
    }
}
