package cz.eman.marekkodr.eman.viewmodel

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.util.Log
import cz.eman.marekkodr.eman.adapter.MyAdapter
import cz.eman.marekkodr.eman.entity.Item
import cz.eman.marekkodr.eman.entity.ItemsResponse
import cz.eman.marekkodr.eman.holder.Loading
import cz.eman.marekkodr.eman.utils.ApiInterface
import retrofit2.Call
import retrofit2.Callback

class MainViewModel : BaseViewModel(){

    var mPage = 1

    var items : MutableList<Item?> =  java.util.ArrayList()
    lateinit var adapter : MyAdapter
    private var loading : Boolean = false

    fun setAdapter(recyclerView: RecyclerView, activity: Activity){

        this.adapter = MyAdapter(activity, items)

        recyclerView.adapter = adapter

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0)
                //check for scroll down
                {
                    loadItems(activity)
                }
            }
        })

        if(items.isEmpty()) {
            loadItems(activity)
        }
    }

    fun loadItems(activity: Activity){
        if(!loading && Loading.has_more) {
            loading = true
            items.add(null)
            adapter.notifyItemInserted(items.size - 1)
            downloadItems(activity)
        }
    }

    private fun downloadItems(activity: Activity) {

        val apiService = ApiInterface.create()

        //set params for request
        val call = apiService.getItems("withbody","desc","creation","cooking",5,mPage)

        call.enqueue(object : Callback<ItemsResponse> {

            override fun onResponse(call: Call<ItemsResponse>, response: retrofit2.Response<ItemsResponse>?) {

                if (response != null) {

                    //if response is not OK
                    if(response.code() != 200){
                        Log.d("REQUEST", "Failed Code: " + response.code())
                        showToastMessage("Loading Failed. Code: " + response.code(), activity)
                        removeLastItem()
                        return
                    }

                    Log.d("REQUEST", "Succeed")

                    removeLastItem()

                    response.body()!!.items!!.forEach { items.add(it) }

                    adapter.notifyDataSetChanged()

                    setFlags(response)

                    //set page achor
                    mPage ++

                    downloadMoreIfNeeded(activity)

                    loading = false
                }
            }

            override fun onFailure(call: Call<ItemsResponse>, t: Throwable) {
                removeLastItem()
                Log.d("REQUEST", "Failed")
                loading = false

            }
        })
    }

    fun removeLastItem() {
        items.removeAt(items.size -1)
        adapter.notifyItemRemoved(items.size)
    }

    fun downloadMoreIfNeeded(activity: Activity){
        if(items.size <= 10){
            items.add(null)
            adapter.notifyItemInserted(items.size - 1)
            downloadItems(activity)
        }
    }

    //read flags for loading
    fun setFlags(response: retrofit2.Response<ItemsResponse>?){
        Loading.has_more = response?.body()?.has_more!!.toBoolean()
        Loading.quota_max = response.body()?.quota_max!!.toInt()
        Loading.quota_remaining = response.body()?.quota_remaining!!.toInt()
    }

}