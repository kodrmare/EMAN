package cz.eman.marekkodr.eman.viewmodel

import android.arch.lifecycle.ViewModel
import cz.eman.marekkodr.eman.holder.Item

class DetailViewModel : ViewModel() {

    fun getItem() : cz.eman.marekkodr.eman.entity.Item {
        return Item.item!!
    }

    fun getOwnerUrl() : String
    {
        return Item.item!!.owner!!.profile_image.toString()
    }
}