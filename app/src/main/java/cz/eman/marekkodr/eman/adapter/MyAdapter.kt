package cz.eman.marekkodr.eman.adapter

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.Html.fromHtml
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.eman.marekkodr.eman.R
import cz.eman.marekkodr.eman.activity.DetailActivity
import cz.eman.marekkodr.eman.activity.MainActivity
import cz.eman.marekkodr.eman.fragment.DetailFragment
import cz.eman.marekkodr.eman.holder.Fragment
import cz.eman.marekkodr.eman.holder.Item
import kotlinx.android.synthetic.main.item_loading.view.*
import kotlinx.android.synthetic.main.item_recycler.view.*
import java.util.*

private const val VIEW_ITEM_TYPE = 0
private const val VIEW_LOADING_TYPE = 1

internal class LoadingViewHolder (itemView : View) : RecyclerView.ViewHolder(itemView) {

    var progressBar = itemView.item_loading_progress!!
}

internal class ItemViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){

    var title  = itemView.item_title!!
    var date = itemView.item_date!!
    var score = itemView.item_score!!
    var image = itemView.item_icon!!
    var layout = itemView.item_body!!
}

class MyAdapter (internal var activity: Activity, private var items:MutableList<cz.eman.marekkodr.eman.entity.Item?>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == VIEW_ITEM_TYPE){
            val view = LayoutInflater.from(activity).inflate(R.layout.item_recycler, parent,false )
            ItemViewHolder(view)
        }
        else {
            val view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent,false )
            LoadingViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if(holder is ItemViewHolder){
            val item = items[position]
            holder.date.text = getDateTime(item?.creation_date!!.toInt())
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.title.text = fromHtml(item.title,Html.FROM_HTML_MODE_LEGACY)
            } else {
                holder.title.text = fromHtml(item.title)
            }

            holder.score.text = item.view_count

            //set drawable according to score(views_count)
            val selectedDrawable : Int = when {
                item.view_count!!.toInt() < 10 -> R.drawable.ic_signal_cellular_0_bar_black_24dp
                item.view_count!!.toInt() < 30 -> R.drawable.ic_signal_cellular_1_bar_black_24dp
                item.view_count!!.toInt() < 60 -> R.drawable.ic_signal_cellular_2_bar_black_24dp
                item.view_count!!.toInt() < 90 -> R.drawable.ic_signal_cellular_3_bar_black_24dp
                else -> R.drawable.ic_signal_cellular_4_bar_black_24dp
            }

            holder.itemView.item_icon.setImageResource(selectedDrawable)

            holder.itemView.setOnClickListener {
                Item.item = item
                if((activity as MainActivity).isTablet(activity)){
                    Fragment.fragment = DetailFragment()
                    (activity as MainActivity).setSideFragment()
                }
                else {
                    val intent = Intent(holder.itemView.context, DetailActivity::class.java)
                    holder.itemView.context.startActivity(intent)
                }
            }
            //later add change color
        } else {
            holder as LoadingViewHolder
            holder.progressBar.isIndeterminate = true
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if(items[position] == null) VIEW_LOADING_TYPE else VIEW_ITEM_TYPE
    }

    private fun getDateTime(s: Int): String? {
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = s * 1000L
        return DateFormat.format("dd.MM.yyyy", cal).toString()
    }

}